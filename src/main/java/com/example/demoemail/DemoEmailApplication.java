package com.example.demoemail;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoEmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoEmailApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {

            if (args.length < 3) {
                System.out.println("Insufficient arguments provided. Skipping execution.");
                return;
            }
            String filePath = args[0];
            String subject = args[1];
            String body = args[2];
            EmailService emailService = ctx.getBean(EmailService.class);
            emailService.sendMailWithAttachment(filePath, subject, body);
            SpringApplication.exit(ctx);
        };
    }
}
