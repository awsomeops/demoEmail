package com.example.demoemail;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import java.io.File;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMailWithAttachment(String filePath, String subject, String body) throws MessagingException {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setFrom("noreply@example.com");
            mimeMessageHelper.setTo(System.getenv("ADMIN_EMAIL"));
            mimeMessageHelper.setText(body);
            mimeMessageHelper.setSubject(subject);

            System.out.println("Attaching file: " + filePath);
            FileSystemResource fileSystemResource = new FileSystemResource(new File(filePath));
            mimeMessageHelper.addAttachment(fileSystemResource.getFilename(), fileSystemResource);

            System.out.println("Sending email...");
            javaMailSender.send(mimeMessage);
            System.out.println("Email sent successfully with attachment: " + filePath);
        } catch (Exception e) {
            System.out.println("Failed to send email: " + e.getMessage());
            e.printStackTrace();
        }
    }
}